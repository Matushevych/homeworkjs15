const startBtn = document.getElementById('start-btn');
const clearBtn = document.getElementById('clear-btn');
const mainContainer = document.getElementById('main');
const pauseBtn = document.createElement('button');

pauseBtn.innerText = "Pause";
pauseBtn.setAttribute("id", "clear-btn");

const hourArrow = document.getElementById("hour-arm");
const minArrow = document.getElementById("minute-arm");
const secArrow = document.getElementById("second-arm");
const millArrow = document.getElementById("milli-second-arm");

let hourVal = 1;
let minVal = 1;
let secVal = 1;
let millVal = 1;

let clearInt;

let startRunning = () =>{
    startBtn.remove();
    mainContainer.insertBefore(pauseBtn, mainContainer.children[0]);

    let hourRun = setInterval(() =>{
        hourArrow.style.transform = `rotateZ(${hourVal*0.01}deg)`;
        hourVal++;
    },12000);
    let minRun = setInterval(() =>{
        minArrow.style.transform = `rotateZ(${minVal*0.1}deg)`;
        minVal++;
    },1000);
    let secRun = setInterval(() =>{
        secArrow.style.transform = `rotateZ(${secVal*0.06}deg)`;
        secVal++;
    },10);
    let millRun = setInterval(() =>{
        millArrow.style.transform = `rotateZ(${millVal*3.6}deg)`;
        millVal++;
    },10);

    clearInt =()=>{
        clearInterval(hourRun);
        clearInterval(minRun);
        clearInterval(secRun);
        clearInterval(millRun);
        pauseBtn.remove();
        mainContainer.insertBefore(startBtn, mainContainer.firstChild);
    };
};

startBtn.addEventListener('click', startRunning);

pauseBtn.addEventListener("click",()=>{clearInt()});

clearBtn.addEventListener("click",()=>{
    hourArrow.style.transform = `rotateZ(0deg)`;
    minArrow.style.transform = `rotateZ(0deg)`;
    secArrow.style.transform = `rotateZ(0deg)`;
    millArrow.style.transform = `rotateZ(0deg)`;
    clearInt();
    hourVal=1;
    minVal=1;
    secVal=1;
    millVal=1;
});